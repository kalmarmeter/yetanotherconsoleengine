#include "stdafx.h"
#include "Console.h"


Console::Console(Size console_size, Size font_size) {

	handle_read = GetStdHandle(STD_INPUT_HANDLE);
	handle_write = GetStdHandle(STD_OUTPUT_HANDLE);

	SetConsoleTitle(TEXT("Console Engine"));

	rect_size = { 0, 0, 1, 1 };
	SetConsoleWindowInfo(handle_write, TRUE, &rect_size);

	buffer_size = { (SHORT)console_size.width, (SHORT)console_size.height };
	SetConsoleScreenBufferSize(handle_write, buffer_size);
	SetConsoleActiveScreenBuffer(handle_write);

	CONSOLE_FONT_INFOEX font_info;
	font_info.cbSize = sizeof(font_info);
	font_info.nFont = 0;
	font_info.dwFontSize.X = font_size.width;
	font_info.dwFontSize.Y = font_size.height;
	font_info.FontFamily = FF_DONTCARE;
	font_info.FontWeight = FW_NORMAL;

	wcscpy_s(font_info.FaceName, L"Consolas");
	SetCurrentConsoleFontEx(handle_write, false, &font_info);

	rect_size = { 0, 0, (SHORT)console_size.width - 1, (SHORT)console_size.height - 1 };
	SetConsoleWindowInfo(handle_write, TRUE, &rect_size);

	DWORD window_mode = ENABLE_EXTENDED_FLAGS | ENABLE_MOUSE_INPUT | ENABLE_WINDOW_INPUT;
	SetConsoleMode(handle_read, window_mode);

	color_text = Color::white;
	color_clear = Color::black || Color::black_bk;

	buffer_console = new CHAR_INFO[console_size.width * console_size.height];

	mouse_state.x = 0;
	mouse_state.y = 0;
	for (int i = 0; i < 3; ++i) {
		mouse_state.button_state[i] = ButtonState::NotPressed;
	}

	clear();

}

bool Console::in(Pos pos) const {

	return (pos.x >= 0 && pos.y >= 0 && pos.x < width() && pos.y < height());

}

void Console::start() {

	start_time = GetTickCount();

}

void Console::stop() {

	end_time = GetTickCount();
	last_frame_time = end_time - start_time;

}

void Console::limit_fps(DWORD target_fps) {

	DWORD target_frame_time = 1000 / target_fps;

	if (target_frame_time > last_frame_time) {
		Sleep(target_frame_time - last_frame_time);
	}

}

DWORD Console::frame_time() const {

	return last_frame_time;

}

int Console::width() const {

	return buffer_size.X;

}

int Console::height() const {

	return buffer_size.Y;

}

Size Console::size() const {

	return Size(width(), height());

}

bool Console::pressed(WORD key) const {

	return keyboard_state[key] == KeyState::Pressed;

}

bool Console::pressed(Key key) const {

	return keyboard_state[(WORD)key] == KeyState::Pressed;

}

bool Console::released(WORD key) const {

	return keyboard_state[key] == KeyState::Released;

}

bool Console::released(Key key) const {

	return keyboard_state[(WORD)key] == KeyState::Released;

}

bool Console::mouse_pressed(MouseButton button) const {

	return (mouse_state.button_state[(int)button] == ButtonState::Pressed);

}

bool Console::mouse_released(MouseButton button) const {

	return (mouse_state.button_state[(int)button] == ButtonState::Released);
	
}

int Console::mouse_x() const {

	return mouse_state.x;

}

int Console::mouse_y() const {

	return mouse_state.y;

}

Pos Console::mouse_pos() const {

	return Pos(mouse_x(), mouse_y());

}

void Console::set_color_clear(Color color) {

	color_clear = color;

}

void Console::set_color_text(Color color) {

	color_text = color;

}

void Console::display() {

	COORD coord_buffer_size = { (SHORT)width(), (SHORT)height() };
	COORD coord_start_pos = { 0, 0 };
	SMALL_RECT rect_area = { 0, 0, (SHORT)width() - 1, (SHORT)height() - 1 };

	WriteConsoleOutputA(handle_write, buffer_console, coord_buffer_size, coord_start_pos, &rect_area);

}

void Console::process_events() {

	for (int i = 0; i < 256; ++i) {

		keyboard_state[i] = KeyState::NotPressed;

	}

	for (int i = 0; i < 3; ++i) {

		if (mouse_state.button_state[i] == ButtonState::Released) {
			mouse_state.button_state[i] = ButtonState::NotPressed;
		}

	}


	DWORD event_num = 0;
	DWORD event_read_num = 0;

	GetNumberOfConsoleInputEvents(handle_read, &event_num);

	if (event_num != 0) {

		INPUT_RECORD* event_buffer = new INPUT_RECORD[event_num];

		ReadConsoleInput(handle_read, event_buffer, event_num, &event_read_num);

		for (DWORD i = 0; i < event_read_num; ++i) {

			if (event_buffer[i].EventType == KEY_EVENT) {

				if (event_buffer[i].Event.KeyEvent.bKeyDown) {

					keyboard_state[event_buffer[i].Event.KeyEvent.wVirtualKeyCode] = KeyState::Pressed;

				} else {

					keyboard_state[event_buffer[i].Event.KeyEvent.wVirtualKeyCode] = KeyState::Released;

				}

			} else if (event_buffer[i].EventType == MOUSE_EVENT) {

				if (event_buffer[i].Event.MouseEvent.dwEventFlags == 0) {

					DWORD bs = event_buffer[i].Event.MouseEvent.dwButtonState;

					if (bs & FROM_LEFT_1ST_BUTTON_PRESSED) {
						mouse_state.button_state[0] = ButtonState::Pressed;
					} else {
						if (mouse_state.button_state[0] == ButtonState::Pressed) {
							mouse_state.button_state[0] = ButtonState::Released;
						}
					}

					if (bs & RIGHTMOST_BUTTON_PRESSED) {
						mouse_state.button_state[1] = ButtonState::Pressed;
					}
					else {
						if (mouse_state.button_state[1] == ButtonState::Pressed) {
							mouse_state.button_state[1] = ButtonState::Released;
						}
					}

					if (bs & FROM_LEFT_2ND_BUTTON_PRESSED) {
						mouse_state.button_state[2] = ButtonState::Pressed;
					}
					else {
						if (mouse_state.button_state[2] == ButtonState::Pressed) {
							mouse_state.button_state[2] = ButtonState::Released;
						}
					}
					
				} else if (event_buffer[i].Event.MouseEvent.dwEventFlags == MOUSE_MOVED) {

					mouse_state.x = event_buffer[i].Event.MouseEvent.dwMousePosition.X;
					mouse_state.y = event_buffer[i].Event.MouseEvent.dwMousePosition.Y;

				}

			}

		}

		delete[] event_buffer;
	}

}

void Console::write_char(char ch, Pos pos) {

	if (in(pos)) {
		buffer_console[pos.x + pos.y * width()].Char.AsciiChar = ch;
		buffer_console[pos.x + pos.y * width()].Attributes = color_text.color | color_clear.color;
	}

}

void Console::write_char_colored(char ch, Color col, Pos pos) {

	if (in(pos)) {
		buffer_console[pos.x + pos.y * width()].Char.AsciiChar = ch;
		buffer_console[pos.x + pos.y * width()].Attributes = col.color;
	}

}

void Console::clear() {

	for (int i = 0; i < width() * height(); ++i) {
		buffer_console[i].Char.AsciiChar = ' ';
		buffer_console[i].Attributes = color_clear.color;
	}

}

void Console::write_text(std::string str, Pos pos) {
	for (int i = 0; i < str.length() && pos.x + i < width(); ++i) {
		write_char(str[i], Pos(pos.x + i, pos.y));
	}
}

void Console::write_text(std::vector<std::string>& in, Pos pos) {
	for (int i = 0; i < in.size(); ++i) {
		write_text(in[i], Pos(pos.x, pos.y + i));
	}

}

void Console::write_text_colored(std::string str, Pos pos, Color col) {
	for (int i = 0; i < str.length() && i + pos.x < width(); ++i) {
		write_char_colored(str[i], col, Pos(pos.x + i, pos.y));
	}

}

void Console::write_text_colored(std::vector<std::string>& in, Pos pos, Color col) {
	for (int i = 0; i < in.size(); ++i) {
		write_text_colored(in[i], Pos(pos.x, pos.y + i), col);
	}

}

void Console::draw_line(Pos pos1, Pos pos2, char ch, Color col) {

	if (in(pos1) && in(pos2)) {

		int dx = pos2.x - pos1.x;
		int dy = pos2.y - pos1.y;

		int steps = max(abs(dx), abs(dy));

		float x_step = (float)dx / (float)steps;
		float y_step = (float)dy / (float)steps;
		
		float x = pos1.x;
		float y = pos1.y;

		for (int s = 0; s < steps; ++s) {
			write_char_colored(ch, col, Pos(x,y));
			x += x_step;
			y += y_step;
		}

	}

}

void Console::draw_rect(Pos pos, Size size, char ch, Color col) {

	if (in(pos) && in(Pos(pos.x + size.width - 1, pos.y + size.height - 1))) {

		for (int i = 0; i < size.width; ++i) {
			for (int j = 0; j < size.height; ++j) {
				if (i == 0 || j == 0 || i == size.width - 1 || j == size.height - 1) {
					write_char_colored(ch, col, Pos(pos.x + i, pos.y + j));
				}
			}
		}

	}

}

void Console::draw_rect(Pos pos1, Pos pos2, char ch, Color col) {

	draw_rect(pos1, Size(pos2.x - pos1.x + 1, pos2.y - pos1.y + 1), ch, col);

}

void Console::draw_rect_filled(Pos pos, Size size, char ch_border, char ch_fill, Color col_border, Color col_fill) {

	if (in(pos) && in(Pos(pos.x + size.width - 1, pos.y + size.height - 1))) {

		for (int i = 0; i < size.width; ++i) {
			for (int j = 0; j < size.height; ++j) {

				if (i == 0 || j == 0 || i == size.width - 1 || j == size.height - 1) {
					write_char_colored(ch_border, col_border, Pos(pos.x + i, pos.y + j));
				} else {
					write_char_colored(ch_fill, col_fill, Pos(pos.x + i, pos.y + j));
				}

			}
		}

	}

}

void Console::draw_rect_filled(Pos pos1, Pos pos2, char ch_border, char ch_fill, Color col_border, Color col_fill) {

	draw_rect_filled(pos1, Size(pos2.x - pos1.x + 1, pos2.y - pos1.y + 1), ch_border, ch_fill, col_border, col_fill);

}

void Console::draw_rect_filled(Pos pos, Size size, char ch, Color col) {

	draw_rect_filled(pos, size, ch, ch, col, col);

}

void Console::draw_rect_filled(Pos pos1, Pos pos2, char ch, Color col) {

	draw_rect_filled(pos1, pos2, ch, ch, col, col);

}

void Console::draw_border(char ch, Color col) {

	draw_rect(Pos(0, 0), size(), ch, col);

}

Console::~Console() {

	delete[] buffer_console;

}
