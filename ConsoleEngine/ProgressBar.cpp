#include "stdafx.h"
#include "ProgressBar.h"


ProgressBar::ProgressBar(Pos pos, Size size, Console* con) : window(pos, size, con) {

	console = con;
	pb_pos = pos;
	pb_size = size;

}

ProgressBar::ProgressBar(Pos pos1, Pos pos2, Console* con) :
	ProgressBar(pos1, Size(pos2.x - pos1.x + 1, pos2.y - pos1.y + 1), con) {}

int ProgressBar::width() const {

	return pb_size.width;

}

int ProgressBar::height() const {

	return pb_size.height;

}

Size ProgressBar::size() const {

	return pb_size;

}

void ProgressBar::set_style_done(char ch, Color col) {

	ch_done = ch;
	col_done = col;

}

void ProgressBar::set_style_empty(char ch, Color col) {

	ch_empty = ch;
	col_empty = col;

}

void ProgressBar::set_show_text(bool value) {

	show_text = value;

}

void ProgressBar::set_value(int val) {

	value = val;

}

void ProgressBar::set_max_value(int max_val) {

	max_value = max_val;

}

void ProgressBar::display() {

	float progress;
	if (max_value <= 0) {
		progress = 0.0f;
	} else {
		progress = min((float)value / (float)max_value, 1.0f);
	}

	int bar_w = (float)width() * progress;

	window.draw_rect_filled(Pos(0, 0), Size(bar_w, height()), ch_done, col_done);
	window.draw_rect_filled(Pos(bar_w, 0), Size(width() - bar_w, height()), ch_empty, col_empty);

	if (show_text) {

		int percentage = progress * 100.0;
		window.write_text(to_string(percentage) + "%", Pos(width() / 2 - 2, height() / 2));
		
	}

}

ProgressBar::~ProgressBar()
{
}
