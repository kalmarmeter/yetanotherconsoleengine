#pragma once

#include <Windows.h>
#include "Color.h"
#include "Keys.h"
#include "MouseState.h"
#include "Util.h"
#include <string>

struct Pos {
	size_t x, y;
	Pos(size_t xx, size_t yy) { x = max(0, xx); y = max(0, yy); }
	Pos() : x(0), y(0) {}
	Pos operator+(const Pos& rhs) { return Pos(x + rhs.x, y + rhs.y); }
	bool operator==(const Pos& rhs) { return x == rhs.x && y == rhs.y; }
};

struct Size {
	size_t width, height;
	Size(size_t w, size_t h) { width = max(0, w); height = max(0, h); }
	Size() : width(0), height(0) {}
	bool operator==(const Size& rhs) { return width == rhs.width && height == rhs.height; }
};

const char fill_1 = 176;
const char fill_2 = 177;
const char fill_3 = 178;
const char fill_4 = 219;

class Console {
private:

	HANDLE		handle_write;
	HANDLE		handle_read;
	SMALL_RECT	rect_size;
	COORD		buffer_size;
	CHAR_INFO*	buffer_console;

	Color color_clear;
	Color color_text;

	KeyState keyboard_state[256];
	MouseState mouse_state;

	DWORD start_time, end_time, last_frame_time;

public:
	Console(Size, Size);

	void start();
	void stop();
	void limit_fps(DWORD);
	DWORD frame_time() const;

	int height() const;
	int width()  const;
	Size size() const;

	bool in(Pos) const;

	bool pressed(WORD) const;
	bool pressed(Key) const;
	bool released(WORD) const;
	bool released(Key) const;
	bool mouse_pressed(MouseButton) const;
	bool mouse_released(MouseButton) const;
	int mouse_x() const;
	int mouse_y() const;
	Pos mouse_pos() const;

	void set_color_clear(Color);
	void set_color_text(Color);

	void display();
	void process_events();
	void clear();

	void write_char(char, Pos);
	void write_char_colored(char, Color, Pos);
	void write_text(std::string, Pos);
	void write_text(std::vector<std::string>&, Pos);
	void write_text_colored(std::string, Pos, Color);
	void write_text_colored(std::vector<std::string>&, Pos, Color);

	void draw_line(Pos, Pos, char, Color);
	void draw_rect(Pos, Pos, char, Color);
	void draw_rect(Pos, Size, char, Color);
	void draw_rect_filled(Pos, Pos, char, char, Color, Color);
	void draw_rect_filled(Pos, Size, char, char, Color, Color);
	void draw_rect_filled(Pos, Pos, char, Color);
	void draw_rect_filled(Pos, Size, char, Color);
	void draw_border(char, Color);

	~Console();
};