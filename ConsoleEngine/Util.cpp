#include "stdafx.h"
#include "Util.h"

std::vector<std::string> split(std::string str, char delim) {

	std::vector<std::string> result;
	std::string tmp;
	for (auto ch : str) {

		if (ch != delim) {
			tmp.push_back(ch);
		}
		else {
			result.push_back(tmp);
			tmp = "";
		}

	}

	return result;

}

void init_random(int seed) {

	srand(seed);

}

int random(int low, int high) {

	if (low > high) return 0;

	int dist = high - low + 1;

	return (rand() % dist + low);

}