#pragma once

#include <Windows.h>

enum class MouseButton {
	Left = 0,
	Right,
	Middle
};

enum class ButtonState {
	NotPressed = 0,
	Pressed,
	Released
};

struct MouseState {

	ButtonState button_state[3];
	size_t x, y;

};