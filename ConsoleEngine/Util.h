#pragma once

#include <sstream>
#include <string>
#include <vector>
#include <ctime>

template <typename T>
std::string to_string(T a) {

	std::stringstream ss;
	ss << a;
	return ss.str();

}

std::vector<std::string> split(std::string str, char delim = ' ');

void init_random(int seed = time(0));
int random(int low, int high);