#pragma once


enum class Key {

	A = 0x41,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z

};

enum class KeyState {

	NotPressed = 0,
	Pressed,
	Released

};