#pragma once

#include <Windows.h>

struct RGB_Color {

	int r, g, b;
	RGB_Color(int _r = 0, int _g = 0, int _b = 0) {

		r = min(_r, 255);
		g = min(_g, 255);
		b = min(_b, 255);

	}

	RGB_Color operator+(const RGB_Color& rhs) const {

		return RGB_Color(r + rhs.r, g + rhs.g, b + rhs.b);

	}

	RGB_Color operator-(const RGB_Color& rhs) const {

		return RGB_Color(r - rhs.r, g - rhs.g, b - rhs.b);

	}

	RGB_Color operator*(float f) const {

		return RGB_Color(r * f, g * f, b * f);

	}

};

struct Color {

	WORD color;

	Color() : color(0) {}
	Color(WORD c) : color(c) {}

	/*
	FOREGROUND COLORS
	*/
	static Color white;
	static Color black;
	static Color red;
	static Color green;
	static Color blue;
	static Color yellow;
	static Color magenta;
	static Color cyan;

	static Color gray;
	static Color dark_gray;
	static Color dark_red;
	static Color dark_green;
	static Color dark_blue;
	static Color dark_yellow;
	static Color dark_magenta;
	static Color dark_cyan;

	/*
	BACKGROUND COLORS
	*/
	static Color white_bk;
	static Color black_bk;
	static Color red_bk;
	static Color green_bk;
	static Color blue_bk;
	static Color yellow_bk;
	static Color magenta_bk;
	static Color cyan_bk;

	static Color gray_bk;
	static Color dark_gray_bk;
	static Color dark_red_bk;
	static Color dark_green_bk;
	static Color dark_blue_bk;
	static Color dark_yellow_bk;
	static Color dark_magenta_bk;
	static Color dark_cyan_bk;

	Color operator||(Color);

};

RGB_Color	color_to_RGB(const Color& color);
Color		RGB_to_color_approx(const RGB_Color& color, int i);
int			color_distance_sq(const RGB_Color& color1, const RGB_Color& color2);

Color		RGB_to_color(const RGB_Color& color);