#pragma once

#include "Console.h"

class Window {
private:

	Console* console;
	Pos w_pos;
	Size w_size;

	bool in(Pos) const;

public:
	Window(Pos, Size, Console*);
	Window(Pos, Pos, Console*);
	
	int width() const;
	int height() const;
	Size size() const;

	void clear();

	void write_char(char, Pos);
	void write_char_colored(char, Color, Pos);
	void write_text(std::string, Pos);
	void write_text(std::vector<std::string>&, Pos);
	void write_text_colored(std::string, Pos, Color);
	void write_text_colored(std::vector<std::string>&, Pos, Color);

	void draw_line(Pos, Pos, char, Color);
	void draw_rect(Pos, Size, char, Color);
	void draw_rect(Pos, Pos, char, Color);
	void draw_rect_filled(Pos, Size, char, char, Color, Color);
	void draw_rect_filled(Pos, Pos, char, char, Color, Color);
	void draw_rect_filled(Pos, Pos, char, Color);
	void draw_rect_filled(Pos, Size, char, Color);
	void draw_border(char, Color);

	~Window();
};

