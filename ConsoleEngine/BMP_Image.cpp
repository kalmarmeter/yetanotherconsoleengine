#include "stdafx.h"
#include "BMP_Image.h"


BMP_Image::BMP_Image(const std::string& filename) {

	FILE* file;
	fopen_s(&file, filename.c_str(), "rb");
	unsigned char info[54];
	fread(info, sizeof(unsigned char), 54, file);

	bmp_size.width = *(int*)&info[18];
	bmp_size.height = *(int*)&info[22];

	data.resize(bmp_size.width * bmp_size.height);
	size_t size = 3 * bmp_size.width * bmp_size.height;
	unsigned char* arr_colors = new unsigned char[size];
	fread(arr_colors, sizeof(unsigned char), size, file);
	fclose(file);

	for (size_t i = 0; i < size; i += 3) {

		unsigned char tmp = arr_colors[i];
		arr_colors[i] = arr_colors[i + 2];
		arr_colors[i + 2] = tmp;

		data[i / 3] = RGB_Color(arr_colors[i], arr_colors[i + 1], arr_colors[i + 2]);

	}

	delete[] arr_colors;

}

RGB_Color BMP_Image::get_rgb(size_t x, size_t y) const {

	if (x >= 0 && x < bmp_size.width && y >= 0 && y < bmp_size.height)	return data.at(x + (bmp_size.width - y - 1) * bmp_size.width);
	else																return RGB_Color();

}

size_t BMP_Image::width() const {
	return bmp_size.width;
}

size_t BMP_Image::height() const {
	return bmp_size.height;
}

Size BMP_Image::size() const {
	return bmp_size;
}

