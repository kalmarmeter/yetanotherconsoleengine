#pragma once

#include <vector>
#include "Color.h"
#include "Console.h"

class BMP_Image {

	Size bmp_size;
	std::vector<RGB_Color> data;

public:
	BMP_Image(const std::string&);
	RGB_Color get_rgb(size_t, size_t) const;
	size_t width() const;
	size_t height() const;
	Size size() const;
};

