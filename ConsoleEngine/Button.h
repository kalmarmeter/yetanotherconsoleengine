#pragma once

#include "Window.h"


class Button {
private:

	Pos b_pos;
	Size b_size;
	Console* console;
	Window window;

	char ch_border_noclick = 219;
	Color col_border_noclick = Color::white;
	Color col_fill_noclick = Color::black;

	char ch_border_hover = 219;
	Color col_border_hover = Color::white;
	Color col_fill_hover = Color::gray;

	char ch_border_click = 219;
	Color col_border_click = Color::dark_red;
	Color col_fill_click = Color::gray;

	std::string text = "";

public:

	Button(Pos, Size, Console*);
	Button(Pos, Pos, Console*);

	int width() const;
	int height() const;
	Size size() const;

	bool mouse_over() const;
	bool clicked() const;
	bool released() const;

	void set_text(std::string);
	void set_style_clicked(char, Color, Color);
	void set_style_hover(char, Color, Color);
	void set_style_not_clicked(char, Color, Color);

	void display();

	~Button();
};

