#include "stdafx.h"
#include "Window.h"


Window::Window(Pos pos, Size size, Console* con) {

	console = con;
	w_pos = pos;
	w_size = size;

}

Window::Window(Pos pos1, Pos pos2, Console* con) :
	Window(pos1, Size(pos2.x - pos1.x + 1, pos2.y - pos1.y + 1), con) {}

int Window::width() const {
	return w_size.width;
}

int Window::height() const {
	return w_size.height;
}

Size Window::size() const {
	return w_size;
}

bool Window::in(Pos pos) const {
	return (pos.x >= 0 && pos.x < width() && pos.y >= 0 && pos.y < height());
}

void Window::clear() {

	for (int i = 0; i < width(); ++i) {
		for (int j = 0; j < height(); ++j) {
			write_char_colored(' ', Color::black || Color::black_bk, Pos(i, j));
		}
	}

}

void Window::write_char(char ch, Pos pos) {

	if (in(pos)) {
		console->write_char(ch, w_pos + pos);
	}

}

void Window::write_char_colored(char ch, Color col, Pos pos) {

	if (in(pos)) {
		console->write_char_colored(ch, col, w_pos + pos);
	}

}

void Window::write_text(std::string str, Pos pos) {
	for (int i = 0; i < str.length() && pos.x + i < width(); ++i) {
		write_char(str[i], Pos(pos.x + i, pos.y));
	}
}

void Window::write_text(std::vector<std::string>& in, Pos pos) {

	for (int i = 0; i < in.size(); ++i) {
		write_text(in[i], Pos(pos.x, pos.y + i));
	}

}

void Window::write_text_colored(std::string str, Pos pos, Color col) {

	for (int i = 0; i < str.length() && i + pos.x < width(); ++i) {
		write_char_colored(str[i], col, Pos(pos.x + i, pos.y));
	}

}

void Window::write_text_colored(std::vector<std::string>& in, Pos pos, Color col) {

	for (int i = 0; i < in.size(); ++i) {
		write_text_colored(in[i], Pos(pos.x ,pos.y + i), col);
	}

}

void Window::draw_line(Pos pos1, Pos pos2, char ch, Color col) {

	if (in(pos1) && in(pos2)) {
		console->draw_line(w_pos + pos1, w_pos + pos2, ch, col);
	}

}

void Window::draw_rect(Pos pos, Size size, char ch, Color col) {

	if (in(pos) && in(Pos(pos.x + size.width - 1, pos.y + size.height - 1))) {
		console->draw_rect(w_pos + pos, size, ch, col);
	}

}

void Window::draw_rect(Pos pos1, Pos pos2, char ch, Color col) {

	draw_rect(pos1, Size(pos2.x - pos1.x + 1, pos2.y - pos1.y + 1), ch, col);

}

void Window::draw_rect_filled(Pos pos, Size size, char ch_border, char ch_fill, Color col_border, Color col_fill) {

	if (in(pos) && in(Pos(pos.x + size.width - 1, pos.y + size.height - 1))) {
		console->draw_rect_filled(w_pos + pos, size, ch_border, ch_fill, col_border, col_fill);
	}

}

void Window::draw_rect_filled(Pos pos1, Pos pos2, char ch_border, char ch_fill, Color col_border, Color col_fill) {

	draw_rect_filled(pos1, Size(pos2.x - pos1.x + 1, pos2.y - pos1.y + 1), ch_border, ch_fill, col_border, col_fill);

}

void Window::draw_rect_filled(Pos pos, Size size, char ch, Color col) {

	draw_rect_filled(pos, size, ch, ch, col, col);

}

void Window::draw_rect_filled(Pos pos1, Pos pos2, char ch, Color col) {

	draw_rect_filled(pos1, pos2, ch, ch, col, col);

}

void Window::draw_border(char ch, Color col) {

	draw_rect(Pos(0, 0), size(), ch, col);

}

Window::~Window()
{
}
