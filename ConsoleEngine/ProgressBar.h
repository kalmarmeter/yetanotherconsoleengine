#pragma once

#include "Window.h"

class ProgressBar {
private:

	Pos pb_pos;
	Size pb_size;
	Window window;
	Console* console;

	int value = 0;
	int max_value = 100;

	char ch_done = 219;
	Color col_done = Color::green;
	char ch_empty = 219;
	Color col_empty = Color::black;

	bool show_text = true;

public:
	ProgressBar(Pos, Size, Console*);
	ProgressBar(Pos, Pos, Console*);

	int width() const;
	int height() const;
	Size size() const;

	void set_value(int);
	void set_max_value(int);

	void set_style_done(char, Color);
	void set_style_empty(char, Color);
	void set_show_text(bool);

	void display();

	~ProgressBar();
};

