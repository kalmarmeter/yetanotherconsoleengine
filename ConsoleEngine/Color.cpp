#include "stdafx.h"
#include "Color.h"

#define RED FOREGROUND_RED
#define GREEN FOREGROUND_GREEN
#define BLUE FOREGROUND_BLUE
#define INT  FOREGROUND_INTENSITY

#define RED_BK BACKGROUND_RED
#define GREEN_BK BACKGROUND_GREEN
#define BLUE_BK BACKGROUND_BLUE
#define INT_BK BACKGROUND_INTENSITY

Color Color::white = RED | GREEN | BLUE | INT;
Color Color::black = 0;
Color Color::red = RED | INT;
Color Color::green = GREEN | INT;
Color Color::blue = BLUE | INT;
Color Color::yellow = RED | GREEN | INT;
Color Color::magenta = RED | BLUE | INT;
Color Color::cyan = BLUE | GREEN | INT;

Color Color::gray = RED | GREEN | BLUE;
Color Color::dark_gray = INT;
Color Color::dark_red = RED;
Color Color::dark_green = GREEN;
Color Color::dark_blue = BLUE;
Color Color::dark_yellow = RED | GREEN;
Color Color::dark_magenta = RED | BLUE;
Color Color::dark_cyan = BLUE | GREEN;

Color Color::white_bk = RED_BK | GREEN_BK | BLUE_BK | INT_BK;
Color Color::black_bk = 0;
Color Color::red_bk = RED_BK | INT_BK;
Color Color::green_bk = GREEN_BK | INT_BK;
Color Color::blue_bk = BLUE_BK | INT_BK;
Color Color::yellow_bk = RED_BK | GREEN_BK | INT_BK;
Color Color::magenta_bk = RED_BK | BLUE_BK | INT_BK;
Color Color::cyan_bk = BLUE_BK | GREEN_BK | INT_BK;

Color Color::gray_bk = RED_BK | GREEN_BK | BLUE_BK;
Color Color::dark_gray_bk = INT_BK;
Color Color::dark_red_bk = RED_BK;
Color Color::dark_green_bk = GREEN_BK;
Color Color::dark_blue_bk = BLUE_BK;
Color Color::dark_yellow_bk = RED_BK | GREEN_BK;
Color Color::dark_magenta_bk = RED_BK | BLUE_BK;
Color Color::dark_cyan_bk = BLUE_BK | GREEN_BK;


Color Color::operator||(Color rhs) {
	return Color(color | rhs.color);
}

RGB_Color color_to_RGB(const Color & color) {

	int r, g, b;

	bool has_r = (color.color & RED);
	bool has_g = (color.color & GREEN);
	bool has_b = (color.color & BLUE);
	bool has_i = (color.color & INT);
	
	r = (has_r + has_i) * has_r * 255 / 2;
	g = (has_g + has_i) * has_g * 255 / 2;
	b = (has_b + has_i) * has_b * 255 / 2;

	if (!(has_r || has_g || has_b) && has_i) {
		r = g = b = 127;
	}

	if ((has_r && has_g && has_b) && has_i) {
		r = g = b = 192;
	}

	return RGB_Color(r, g, b);

}

Color RGB_to_color_approx(const RGB_Color& color, int i) {

	Color result;

	if (color.r >= (1 + i) * 255 / 4) result.color |= RED;
	if (color.g >= (1 + i) * 255 / 4) result.color |= GREEN;
	if (color.b >= (1 + i) * 255 / 4) result.color |= BLUE;

	if (i) result.color |= INT;

	return result;

}

int color_distance_sq(const RGB_Color& color1, const RGB_Color& color2) {

	return ((color1.r - color2.r) * (color1.r - color2.r) + (color1.g - color2.g) * (color1.g - color2.g) + (color1.b - color2.b) * (color1.b - color2.b));

}

Color RGB_to_color(const RGB_Color& color) {

	Color approx0 = RGB_to_color_approx(color, 0);
	Color approx1 = RGB_to_color_approx(color, 1);

	RGB_Color approx_rgb0 = color_to_RGB(approx0);
	RGB_Color approx_rgb1 = color_to_RGB(approx1);

	float dist0 = color_distance_sq(color, approx_rgb0);
	float dist1 = color_distance_sq(color, approx_rgb1);

	if (dist0 < dist1)  return approx0;
	else				return approx1;

}