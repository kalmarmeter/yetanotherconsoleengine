#include "stdafx.h"
#include "Image.h"


Image::Image(const std::string& filename) {

	BMP_Image bmp(filename);
	im_size = bmp.size();

	std::vector<RGB_Color> colors;
	colors.resize(im_size.width * im_size.height);

	for (size_t x = 0; x < im_size.width; ++x) {
		for (size_t y = 0; y < im_size.height; ++y) {
			colors[x + y * im_size.width] = bmp.get_rgb(x, y);
		}
	}

	data.resize(im_size.width * im_size.height);

	//FLOYD-STEINBERG
	for (size_t y = 0; y < im_size.height; ++y) {
		for (size_t x = 0; x < im_size.width; ++x) {

			RGB_Color old_color_rgb = colors[x + y * im_size.width];
			if (old_color_rgb.r < 0) old_color_rgb.r = 0;
			if (old_color_rgb.g < 0) old_color_rgb.g = 0;
			if (old_color_rgb.b < 0) old_color_rgb.b = 0;

			Color new_color = RGB_to_color(old_color_rgb);
			RGB_Color new_color_rgb = color_to_RGB(new_color);

			RGB_Color err_color = old_color_rgb - new_color_rgb;

			data[x + y * im_size.width] = new_color;

			if (x < im_size.width - 1)									colors[x + 1 + y * im_size.width]		= colors[x + 1 + y * im_size.width] + err_color * (7.0f / 16.0f);
			if (x > 0 && y < im_size.height - 1)						colors[x - 1 + (y + 1) * im_size.width] = colors[x - 1 + (y + 1) * im_size.width] + err_color * (3.0f / 16.0f);
			if (y < im_size.height - 1)									colors[x + (y + 1) * im_size.width]		= colors[x + (y + 1) * im_size.width] + err_color * (5.0f / 16.0f);
			if (x < im_size.width - 1 && y < im_size.height - 1)		colors[x + 1 + (y + 1) * im_size.width] = colors[x + 1 + (y + 1) * im_size.width] + err_color * (1.0f / 16.0f);

		}
	}

}

Color Image::get(size_t x, size_t y) const {

	if (x >= 0 && y >= 0 && x < im_size.width && y < im_size.height) return data[x + y * im_size.width];
	else															 return Color();

}

size_t Image::width() const {
	return im_size.width;
}

size_t Image::height() const {
	return im_size.height;
}

Size Image::size() const {
	return im_size;
}
