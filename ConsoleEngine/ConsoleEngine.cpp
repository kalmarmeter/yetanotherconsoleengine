// ConsoleEngine.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#define PI 3.1416f
#define HALF_PI 1.5708f
#define QUARTER_PI 0.7854f
#define THREE_QUARTER_PI 2.3562f


const int width = 50;
const int height = 50;
const int window_width = 200;
const int window_height = 200;
const int max_steps = 600;
const float step_size = 0.05f;

float player_x, player_y;
float angle = 0.0f;
float fov = PI / 2.5f;

const size_t num_of_threads = 12;

Image wall_tex("wall.bmp");

RGB_Color screen[window_width][window_height];
struct { Color col; char ch; }	  final_screen[window_width][window_height];
std::vector<int> map;

void ray_sim(int thread_id, int num_of_threads) {

	for (int i = 0; i < window_width; ++i) {

		if (i % num_of_threads == thread_id) {

			float fov_step = fov / window_width;
			float ray_angle = angle - (fov / 2.0f) + i * fov_step;

			float rx = player_x, ry = player_y;
			int steps = 0;

			float ray_cos = step_size * cosf(ray_angle);
			float ray_sin = step_size * sinf(ray_angle);

			while (steps < max_steps && (int)rx >= 0 && (int)ry >= 0 && (int)rx < width && (int)ry < height && map[(int)rx + (int)ry * width]) {

				rx += ray_cos;
				ry += ray_sin;
				++steps;
			}

			float width_sample = -1.0f;
			Color color;
			char fill;

			if (steps < max_steps) {

				float center_x = floorf(rx) + 0.5f;
				float center_y = floorf(ry) + 0.5f;

				float angle = atan2f(ry - center_y, rx - center_x);

				if (angle <= QUARTER_PI && angle > -QUARTER_PI) {
					width_sample = ry - floor(ry);
					fill = fill_4;
				}
				else if (angle > QUARTER_PI && angle <= THREE_QUARTER_PI) {
					width_sample = 1.0f - (rx - floor(rx));
					fill = fill_3;
				}
				else if (angle > THREE_QUARTER_PI || angle <= -THREE_QUARTER_PI) {
					width_sample = 1.0f - (ry - floor(ry));
					fill = fill_2;
				}
				else {
					width_sample = rx - floor(rx);
					fill = fill_1;
				}

			}

			float dist_sqr = powf(rx - player_x, 2.0f) + powf(ry - player_y, 2.0f);
			if (dist_sqr < 0.01f) dist_sqr = 0.1f;

			int wall_h = (1.0f / sqrtf(dist_sqr)) * window_height;
			if (wall_h > window_height) wall_h = window_height;
			int floor_height = (window_height - wall_h) / 2;
			for (int j = 0; j < floor_height; ++j) {
				//console.write_char(' ', Pos(i, j));
				final_screen[i][j].col = Color::white;
				final_screen[i][j].ch = ' ';
			}
			size_t sample_x = width_sample * wall_tex.width();
			for (int j = floor_height; j < window_height - floor_height; ++j) {
				size_t sample_y = (j - floor_height) / (float)wall_h * wall_tex.height();
				//console.write_char_colored(fill_4, wall_tex.get(sample_x, sample_y), Pos(i, j));
				final_screen[i][j].col = wall_tex.get(sample_x, sample_y);
				final_screen[i][j].ch = fill;
				//console.write_char_colored(fill_4, color, Pos(i, j));
			}
			for (int j = window_height - floor_height; j < window_height; ++j) {
				//console.write_char(fill_1, Pos(i, j));
				final_screen[i][j].col = Color::white;
				final_screen[i][j].ch = ' ';
			}
		}
	}

}

#define GAME_MODE

int main()
{
	#ifdef GAME_MODE	

	Console console(Size(window_width, window_height), Size(8, 4));

	map.resize(width * height, 0);

	for (int i = 0; i < width * height; ++i) {
		map[i] = rand() % 4;
	}

	bool changed = true;
	player_x = width / 2 + 1;
	player_y = height / 2;


	while (true) {

		console.start();
		console.clear();
		console.process_events();

		if (console.pressed(Key::W)) {

			player_x += 0.1f * cosf(angle);
			player_y += 0.1f * sinf(angle);
			if (!map[(int)player_x + (int)player_y * width]) {
				player_x -= 0.1f * cosf(angle);
				player_y -= 0.1f * sinf(angle);
			}
			changed = true;
		}

		if (console.pressed(Key::S)) {

			player_x -= 0.1f * cosf(angle);
			player_y -= 0.1f * sinf(angle);
			if (!map[(int)player_x + (int)player_y * width]) {
				player_x += 0.1f * cosf(angle);
				player_y += 0.1f * sinf(angle);
			}
			changed = true;
		}

		if (console.pressed(Key::Q)) {

			player_x += 0.1f * cosf(angle);
			player_y -= 0.1f * sinf(angle);
			if (!map[(int)player_x + (int)player_y * width]) {
				player_x -= 0.1f * sinf(angle);
				player_y += 0.1f * cosf(angle);
			}
			changed = true;
		}

		if (console.pressed(Key::E)) {

			player_x -= 0.1f * cosf(angle);
			player_y += 0.1f * sinf(angle);
			if (!map[(int)player_x + (int)player_y * width]) {
				player_x += 0.1f * sinf(angle);
				player_y -= 0.1f * cosf(angle);
			}
			changed = true;
		}

		if (console.pressed(Key::A)) {

			angle -= 0.05f;
			changed = true;

		}
		if (console.pressed(Key::D)) {

			angle += 0.05f;
			changed = true;

		}

		if (changed) {
			
				
			std::vector<std::thread> threads;
			for (int i = 0; i < num_of_threads; ++i)
				threads.push_back(std::thread(ray_sim, i, num_of_threads));
			for (int i = 0; i < num_of_threads; ++i)
				threads[i].join();

			changed = false;
		}

		for (int i = 0; i < window_width; ++i) {
			for (int j = 0; j < window_height; ++j) {
				console.write_char_colored(final_screen[i][j].ch, final_screen[i][j].col, Pos(i, j));
			}
		}

		console.display();
		console.stop();

		console.limit_fps(60);


	}
	/*
	#else
	Console console(Size(400, 400), Size(2, 2));

	Image test("test.bmp");

	while (true) {

		console.start();
		console.clear();
		console.process_events();

		for (int i = 0; i < test.width(); ++i) {
			for (int j = 0; j < test.height(); ++j) {

				Color col = test.get(i, j);
				console.write_char_colored(fill_4, test.get(i, j), Pos(i, j));

			}
		}
		//Color col = test.get(console.mouse_x(), console.mouse_y());
		//console.write_text(to_string(col.color & FOREGROUND_RED) + " " + to_string(col.color & FOREGROUND_GREEN) + " " + to_string(col.color & FOREGROUND_BLUE) + " " + to_string(col.color & FOREGROUND_INTENSITY), Pos(0,0));

		console.display();
		console.stop();
		console.limit_fps(60);

	}
	*/
	#endif
	/*
	Console console(Size(200, 200), Size(4, 4));
	BMP_Image image("wall2.bmp");
	
	struct Colored_Char { char fill; Color color; };
	struct CC_RGB_Pair { Colored_Char cc; RGB_Color rgb; };

	std::vector<CC_RGB_Pair> all_pairs;

	for (int r = 0; r < 2; ++r) {
		for (int g = 0; g < 2; ++g) {
			for (int b = 0; b < 2; ++b) {
				
				Color color;
				if (r) color = color || Color::red;
				if (g) color = color || Color::green;
				if (b) color = color || Color::blue;
					
				all_pairs.push_back({ {fill_1, color}, RGB_Color(r * 29, g * 29, b * 29) });
				all_pairs.push_back({ {fill_2, color}, RGB_Color(r * 101, g * 101, b *101) });
				all_pairs.push_back({ {fill_3, color}, RGB_Color(r * 228, g * 228, b * 228) });
				all_pairs.push_back({ {fill_4, color}, RGB_Color(r * 255, g * 255, b * 255) });


			}
		}
	}
	std::vector<Colored_Char> col_image;
	for (size_t y = 0; y < image.height(); ++y) {
		for (size_t x = 0; x < image.width(); ++x) {


			//CLOSEST COLOR
			RGB_Color rgb_col = image.get_rgb(x, y);
			Colored_Char closest = all_pairs[0].cc;
			int distance = color_distance_sq(all_pairs[0].rgb, rgb_col);
			for (size_t i = 1; i < all_pairs.size(); ++i) {
				int new_dist = color_distance_sq(all_pairs[i].rgb, rgb_col);
				if (new_dist < distance) {
					distance = new_dist;
					closest = all_pairs[i].cc;
				}
			}
			col_image.push_back(closest);

		}
	}

	while (true) {

		console.clear();
		console.start();
		console.process_events();
		for (size_t x = 0; x < image.width(); ++x) {
			for (size_t y = 0; y < image.height(); ++y) {
				console.write_char_colored(col_image[x + y * image.width()].fill, col_image[x + y * image.width()].color, Pos(x, y));
			}
		}

		console.display();
		console.stop();
		console.limit_fps(60);

	}
	*/
    return 0;
}

