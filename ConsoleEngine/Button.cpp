#include "stdafx.h"
#include "Button.h"


Button::Button(Pos pos, Size size, Console* con) : window(pos, size, con) {

	b_pos = pos;
	b_size = size;
	console = con;

}

Button::Button(Pos pos1, Pos pos2, Console* con) :
	Button(pos1, Size(pos2.x - pos1.x + 1, pos2.y - pos1.y + 1), con) {}

int Button::width() const {

	return b_size.width;

}

int Button::height() const {

	return b_size.height;

}

Size Button::size() const {

	return b_size;

}

bool Button::mouse_over() const {

	size_t mouse_x = console->mouse_x();
	size_t mouse_y = console->mouse_y();
	return (mouse_x >= b_pos.x && mouse_x < b_pos.x + width() && mouse_y >= b_pos.y && mouse_y < b_pos.y + height());

}

bool Button::clicked() const {
	
	size_t mouse_x = console->mouse_x();
	size_t mouse_y = console->mouse_y();
	return (mouse_over() && console->mouse_pressed(MouseButton::Left));

}

bool Button::released() const {

	size_t mouse_x = console->mouse_x();
	size_t mouse_y = console->mouse_y();
	return (mouse_over() && console->mouse_released(MouseButton::Left));

}

void Button::set_style_not_clicked(char ch_border, Color col_border, Color col_fill) {

	ch_border_noclick = ch_border;
	col_border_noclick = col_border;
	col_fill_noclick = col_fill;

}

void Button::set_style_hover(char ch_border, Color col_border, Color col_fill) {

	ch_border_hover = ch_border;
	col_border_hover = col_border;
	col_fill_hover = col_fill;

}

void Button::set_style_clicked(char ch_border, Color col_border, Color col_fill) {

	ch_border_click = ch_border;
	col_border_click = col_border;
	col_fill_click = col_fill;

}

void Button::set_text(std::string str) {

	text = str;

}

void Button::display() {

	Color col_border, col_fill;
	char ch_border;

	if (clicked()) {

		col_border = col_border_click;
		col_fill = col_fill_click;
		ch_border = ch_border_click;

	}
	else if (mouse_over()) {

		col_border = col_border_hover;
		col_fill = col_fill_hover;
		ch_border = ch_border_hover;

	} else {

		col_border = col_border_noclick;
		col_fill = col_fill_noclick;
		ch_border = ch_border_noclick;

	}

	window.draw_rect_filled(Pos(0, 0), Size(width(), height()), ch_border, 219, col_border, col_fill);
	console->write_text(text, Pos(b_pos.x + (width() / 2) - (text.size() / 2), b_pos.y + (height() / 2)));

}

Button::~Button()
{
}
