#pragma once

#include "BMP_Image.h"

class Image {

	Size im_size;
	std::vector<Color> data;

public:
	Image(const std::string&);
	Color get(size_t, size_t) const;
	size_t width() const;
	size_t height() const;
	Size size() const;
};

